using SSDLC_DOTNET;
using System.Diagnostics.CodeAnalysis;
namespace TEST_PROJECT
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal(1, 1);
        }

        [Fact]
        public void Test2()
        {
            Assert.Equal(2, 2);
        }

        [Fact]
        public void Test3()
        {
            var classAInstance = new ClassA();
            Assert.Equal(1, classAInstance.MyPropertyA);
        }

        [Fact]
        public void Test4()
        {
            var classAInstance = new ClassA();
            Assert.Equal(2, classAInstance.MyPropertyB);
        }

        [Fact]
        public void Test5()
        {
            var classAInstance = new ClassA();
            Assert.Equal(1, classAInstance.MethodReturns1());
        }
    }
}