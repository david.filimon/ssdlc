﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSDLC_DOTNET
{
    public class ClassA
    {
        public int MyPropertyA { get; set; } = 1;
        public int MyPropertyB { get; set; } = 2;

        public int MethodReturns1() => 1;

    }
}
